package vollgeladen;

public class hotelKlasse {
	private int streckenkilometer;
	private double bewertung;

	hotelKlasse() {

	}

	public hotelKlasse(int streckenkilometer, double bewertung) {
		this.streckenkilometer = streckenkilometer;
		this.bewertung = bewertung;
	}

	public int getStreckenkilometer() {
		return streckenkilometer;
	}

	public void setStreckenkilometer(int streckenkilometer) {
		this.streckenkilometer = streckenkilometer;
	}

	public double getBewertung() {
		return bewertung;
	}

	public void setBewertung(double bewertung) {
		this.bewertung = bewertung;
	}
}
