package vollgeladen;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Vollgeladen {
	public static ArrayList<hotelKlasse> hotels = einlesen(wahl());
	public static ArrayList<hotelKlasse> bestHotels = new ArrayList<hotelKlasse>();

	public static int anzHotels = hotels.size();
	public static int streckeInsgesamt;

	public static void main(String[] args) {
		System.out.println(hotAusgeben());
		// System.out.println(letztesHotelErreichbar(451, 1));
		//System.out.println(letztesHotelErreichbar());
	}

	public static boolean hotAusgeben() {
		// letztesHotelErreichbar();

		int letzteEntfernung = 0;
		ArrayList<hotelKlasse> bestehotels = new ArrayList<hotelKlasse>();

		for (int i = 0; i < 4; i++) {
			ArrayList<hotelKlasse> hInRange = new ArrayList<hotelKlasse>();
			for (int j = 0; j < hotels.size(); j++) {
				if (hotels.get(j).getStreckenkilometer() > letzteEntfernung
						&& hotels.get(j).getStreckenkilometer() <= letzteEntfernung + (60 * 6)) {
					hInRange.add(hotels.get(j));
				}
			}

			hotelKlasse bestesHotel = new hotelKlasse(0, 0);
			double besteWertung = 0.0;
			int nInHotels = 0;

			for (int k = 0; k < hInRange.size(); k++) {
				if (hInRange.get(k).getBewertung() >= besteWertung
						&& hInRange.get(k).getStreckenkilometer() > letzteEntfernung
						&& hInRange.get(k).getStreckenkilometer() <= letzteEntfernung + (60 * 6)) {
					// System.out.println("if");

					// bestehotels.add(bestesHotel);

					/*
					 * for (int j = 0; j < hotels.size(); j++) { if
					 * (bestesHotel.getStreckenkilometer() == hotels.get(j).getStreckenkilometer()
					 * && bestesHotel.getBewertung() == hotels.get(j).getBewertung()) { nInHotels =
					 * j; System.out.println(nInHotels); } }
					 */

					if (letztesHotelErreichbar(hInRange.get(k).getStreckenkilometer(), (i + 1))) {
						//System.out.println("Erreichbar? " + letztesHotelErreichbar(letzteEntfernung, i));
						//System.out.println("Entfernung " + hInRange.get(k).getStreckenkilometer());
						//System.out.println("Durchlauf " + i);
						besteWertung = hInRange.get(k).getBewertung();
						bestesHotel = hInRange.get(k);

					}
					

				}

			}

			if (bestesHotel.getBewertung() != 0.0) {
				System.out.println((i + 1) + ". Hotel: " + bestesHotel.getStreckenkilometer() + "km, "
						+ bestesHotel.getBewertung() + " Sterne");
				System.out.println("----------------------");
				letzteEntfernung = bestesHotel.getStreckenkilometer();

			}

		}
//		if (bestehotels.get(bestehotels.size() - 1).getStreckenkilometer() < (streckeInsgesamt - 360)) {
//			System.out.println("NICHT ERREICHT!");
//		}

		if ((streckeInsgesamt - letzteEntfernung) <= 360) {
			return true;
		}

		return false;
	}

	public static int wHotel(hotelKlasse hot) {
		for (int j = 0; j < hotels.size(); j++) {
			if (hot.getStreckenkilometer() == hotels.get(j).getStreckenkilometer()
					&& hot.getBewertung() == hotels.get(j).getBewertung()) {

				System.out.println(j);
				return j;
			}
		}
		return 0;

	}

	public static boolean letztesHotelErreichbar() {
		int last_pos = 0;
		if (hotels.get(0).getStreckenkilometer() > 360) {
			System.out.println("Hotel ist nicht erreichbar!");
			return false;
		}
		for (int i = 1; i <= 4; i++) {
			ArrayList<hotelKlasse> range = new ArrayList<hotelKlasse>();
			for (int k = 0; k < hotels.size(); k++) {
				if (hotels.get(k).getStreckenkilometer() > last_pos
						&& hotels.get(k).getStreckenkilometer() <= (last_pos + (60 * 6))) {
					range.add(hotels.get(k));
				}
			}
			hotelKlasse Hi = new hotelKlasse();
			int x = range.size() - 1;
			Hi = range.get(x);
			// System.out.println(Hi.getStreckenkilometer());
			last_pos = Hi.getStreckenkilometer();
			if ((streckeInsgesamt - last_pos) <= 360) {
				System.out.println(last_pos);
				return true;
			}
			System.out.println(last_pos);
		}

		return false;
	}

	public static boolean letztesHotelErreichbar(int lastpos, int durchlauf) {
		// System.out.println("lastpos " + lastpos);
		// System.out.println("nHotel " + nHotel);
		// System.out.println("durchlauf " + durchlauf + "");
		int last_pos = lastpos;
		if (durchlauf == 4) {
			if ((streckeInsgesamt - last_pos) <= 360) {
				return true;
			}
		}
		if (hotels.get(0).getStreckenkilometer() > 360) {
			System.out.println("Hotel ist nicht erreichbar!");
			return false;
		}
		for (int i = durchlauf; i < 4; i++) {
			ArrayList<hotelKlasse> range = new ArrayList<hotelKlasse>();
			for (int k = 0; k < hotels.size(); k++) {
				if (hotels.get(k).getStreckenkilometer() > last_pos
						&& hotels.get(k).getStreckenkilometer() <= (last_pos + (60 * 6))) {
					range.add(hotels.get(k));
				}
			}
			hotelKlasse Hi = new hotelKlasse();
			int x = range.size() - 1;

			Hi = range.get(x);
			last_pos = Hi.getStreckenkilometer();
			if ((streckeInsgesamt - last_pos) <= 360) {
				return true;
			}

		}
		return false;
	}

	public static ArrayList<hotelKlasse> einlesen(String datName) {
		ArrayList<hotelKlasse> arr = new ArrayList<hotelKlasse>();

		try {
			File myFile = new File(datName);
			Scanner input = new Scanner(myFile);

			input.nextInt();
			input.nextLine();
			Vollgeladen.streckeInsgesamt = input.nextInt();
			input.nextLine();

			while (input.hasNextLine() && input.hasNextInt()) {
				int streckenkilometer = input.nextInt();
				double bew = Double.valueOf(input.next());
				arr.add(new hotelKlasse(streckenkilometer, bew));
			}
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fehler!");
			e.printStackTrace();
		}
		return arr;
	}

	public static String wahl() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben sie bite als erstes eine Einzulesende Datei ein, oder die Zahl in den Klammern: ");
		System.out.println("Möglichkeiten:  " + "hotels1.txt oder [1]");
		System.out.println("\t\t" + "hotels2.txt oder [2]");
		System.out.println("\t\t" + "hotels3.txt oder [3]");
		System.out.println("\t\t" + "hotels4.txt oder [4]");
		System.out.println("\t\t" + "hotels5.txt oder [5]");

		String datei = sc.next();
		sc.close();
		switch (datei) {
		case "1":
			datei = "hotels1.txt";
			break;
		case "2":
			datei = "hotels2.txt";
			break;
		case "3":
			datei = "hotels3.txt";
			break;
		case "4":
			datei = "hotels4.txt";
			break;
		case "5":
			datei = "hotels5.txt";
			break;
		case "6":
			datei = "test.txt";
			break;
		case "7":
			datei = "test2.txt";
			break;

		default:
			System.out.println("Falsche eingabe, bitte Programm neu starten");
			System.exit(0);
			break;
		}
		return datei;
	}
}
